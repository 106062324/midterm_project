# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Chat room]
* Key functions (add/delete)
    1. [多人聊天]
    2. [單人聊天]
    3. [載入聊天紀錄]
    
* Other functions (add/delete)
    1. [更換密碼]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://chatroom-4e5a9.firebaseapp.com]

# Components Description : 
1. 多人聊天
    >可與所有註冊用戶進行聊天
2. 單人聊天 
    >可由左邊選單選擇聊天對象
3. 載入聊天紀錄
    >根據選擇對象載入聊天紀錄

# Other Functions Description(1~10%) : 
1. 更換密碼
    >點擊左下角change password會自動寄送更改密碼信件至使用者信箱中

## Security Report (Optional)
1. database rule
    >確保僅在認證使用者時能讀/寫資料
2. 防止輸入HTML
    >偵測 html tag 並 escape
