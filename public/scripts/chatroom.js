function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        var profile_name = document.getElementById("profile-name");
        if (user) {
            user_email = user.email;
            profile_name.innerHTML = user_email; 
            postsRef = firebase.database().ref('GroupChat');
            var btnLogOut = document.getElementById('settings');
            btnLogOut.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                }).catch(function(error) {
                    var errorMessage = error.message
                    create_alert("error", errorMessage);
                });
            });
            var contactsRef = firebase.database().ref('contact_list');
            var flag = 0;
            contactsRef.once('value')
                .then(function(snapshot) {
                    for(let i in snapshot.val()) {
                        if(user.email == snapshot.val()[i].email) {
                            flag = 1;
                        } 
                    }
                    if(!flag) {
                        var contact ={
                            email: user.email,
                            userName: "",
                            status: "online"
                        };
                        contactsRef.push(contact);
                    }
                    else {
                        console.log('user exist');
                    }
                })
                .catch(e => console.log(e.message));
        } else {
            window.location.href="index.html";
            console.log("logout");
        }
        
    });

    sent_btn = document.getElementById('sent_btn');
    sent_message = document.getElementById('sent_message');

    sent_btn.addEventListener('click', function () {
        if (sent_message.value != "") {
            var user = firebase.auth().currentUser;
            if(user)
            {
                var message ={
                    email: user.email,
                    comment: htmlspecialchars(sent_message.value)
                };
                postsRef.push(message);
                notice();
                sent_message.value = "";
            }
            else
            {
                create_alert("error", "No User");
            }
        }
    });

    var str_before_contact_status = "<li class='contact'><div class='wrap'><span class='contact-status ";
    var str_after_contact_status =  "'></span><img src='images/unknown-icon.png' alt='' /><div class='meta'><p class='name'>";
    var str_after_contact = "</p><p class='preview'></p></div></div></li>";

    var contactsRef = firebase.database().ref('contact_list');
    var total_contact = [];
    // Counter for checking history post update complete
    var contact_first_count = -1;
    // Counter for checking when to update new post
    var contact_second_count = 0;

    contactsRef.once('value')
        .then(function(snapshot) {
            var contactList = document.getElementById("contact_list");
            contactsRef.on('value', function(snapshot) {
                var user = firebase.auth().currentUser;
                contact_second_count = 0;
                for(let i in snapshot.val()) {
                    if(contact_first_count < contact_second_count) {
                        contact_first_count++;
                        total_contact.push((str_before_contact_status + snapshot.val()[i].status + str_after_contact_status
                            + snapshot.val()[i].email + str_after_contact
                        ));
                        if(snapshot.val()[i].email != user.email)
                            contactList.innerHTML += total_contact[contact_first_count];
                    }
                    contact_second_count++;
                }

            var reply_name = document.getElementById("contact-name");
            var contacts_list = document.getElementById("contacts");
            var contacts = contacts_list.getElementsByClassName("contact");

            for (var i = 0; i < contacts.length; i++) {
                contacts[i].addEventListener("click", function() {
                    var current = contacts_list.getElementsByClassName("active");
                    current_friend = current[0];
                    current[0].className = "contact";
                    this.className += " active";
                    var current_name = this.getElementsByClassName("name");
                    var messageList = document.getElementById('message_list');
                    reply_name.innerHTML = current_name[0].innerText;
                    messageList.innerHTML = "";
                    if(user.email.localeCompare(reply_name.innerHTML) > 0) {
                        postsRef_name = user.email + reply_name.innerHTML;
                    }
                    else {
                        postsRef_name = reply_name.innerHTML + user.email;   
                    }
                    if(current_name[0].innerText != 'Group Chat')
                        postsRef = firebase.database().ref(postsRef_name.replace(/\./g,''));
                    else
                        postsRef = firebase.database().ref('GroupChat');
                    first_count = -1;
                    var total_post = [];
                    //$(".messages").animate({ scrollTop: 0 }, "fast");  
                    postsRef.once('value')
                        .then(function(snapshot) {
                            postsRef.on('value', function(snapshot) {
                            var messageList = document.getElementById('message_list');
                            var user = firebase.auth().currentUser;
                            var reply_name = document.getElementById("contact-name");
                            second_count = 0;
                            for(let i in snapshot.val()) {
                                if(first_count < second_count) {
                                    first_count++;
                                    if(snapshot.val()[i].email == user.email) {
                                        total_post.push((str_before_sent + snapshot.val()[i].comment + str_after_sent));
                                        messageList.innerHTML += total_post[first_count];
                                    }
                                    else if(snapshot.val()[i].email == reply_name.innerHTML || reply_name.innerHTML == 'Group Chat') {
                                        total_post.push((str_before_reply + snapshot.val()[i].comment + str_after_reply));
                                        messageList.innerHTML += total_post[first_count];
                                    }
                                    else
                                        total_post.push((str_before_reply + snapshot.val()[i].comment + str_after_reply));
                                }
                                second_count++;
                            }
                            
                            var messages = document.getElementsByClassName('messages');
                            $(".messages").animate({ scrollTop: messages[0].scrollHeight }, "fast");
                            });
                        })
                        .catch(e => console.log(e.message));
                    if(reply_name.innerHTML == 'Group Chat') {
                        document.getElementById('contact-img').src="images/group-icon.png";
                    }
                    else {
                        document.getElementById('contact-img').src="images/unknown-icon.png";  
                    }        
                });
            }
        })
    })
    .catch(e => console.log(e.message));

    // The html code for message
    var str_before_sent = "<li class='sent'><img src='images/unknown-icon.png' alt=''/><p>";
    var str_after_sent = "</p></li>";
    var str_before_reply = "<li class='replies'><img src='images/unknown-icon.png' alt=''/><p>";
    var str_after_reply = "</p></li>";

    var postsRef_name = 'GroupChat';
    var postsRef = firebase.database().ref('GroupChat');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = -1;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) { postsRef.on('value', function(snapshot) {
            var messageList = document.getElementById('message_list');
                var user = firebase.auth().currentUser;
                var reply_name = document.getElementById("contact-name");
                second_count = 0;
                for(let i in snapshot.val()) {
                    if(first_count < second_count) {
                        first_count++;
                        if(snapshot.val()[i].email == user.email && reply_name.innerHTML == "Group Chat") {
                            total_post.push((str_before_sent + snapshot.val()[i].comment + str_after_sent));
                            messageList.innerHTML += total_post[first_count];
                        }
                        else if(reply_name.innerHTML == "Group Chat") {
                            total_post.push((str_before_reply + snapshot.val()[i].comment + str_after_reply));
                            messageList.innerHTML += total_post[first_count];
                        }
                        else
                            total_post.push((str_before_reply + snapshot.val()[i].comment + str_after_reply));
                    }
                    second_count++;
                }
                var messages = document.getElementsByClassName('messages');
                console.log(messages[0].scrollTop);
                $(".messages").animate({ scrollTop: messages[0].scrollHeight  }, "fast");
            });
        })
        .catch(e => console.log(e.message));
    var changePasswordbtn = document.getElementById('addcontact');
    changePasswordbtn.addEventListener('click', function(){
        pwdchange();
    });
}

window.onload = function () {
    init();
};

function htmlspecialchars(ch) {
    if (ch===null) return '';
    ch = ch.replace(/&/g,"&amp;");
    ch = ch.replace(/\"/g,"&quot;");
    ch = ch.replace(/\'/g,"&#039;");
    ch = ch.replace(/</g,"&lt;");
    ch = ch.replace(/>/g,"&gt;");
    return ch;
}

function notice() {
    var notification;
    if (Notification.permission != "granted") {
        Notification.requestPermission().then(function(result) {
            if(result=='denied') console.log('deny');
            else if(result=='granted') {
                console.log("granted");
                notification = new Notification("New Message!",{
                    body:"Check it now!"
                })
            }
        })
    }
    else {
        notification = new Notification("New Message?",{body:"Check Now!"})
    }
}

function pwdchange(){
    var auth = firebase.auth();
    var emailAddress = auth.currentUser.email;

    auth.sendPasswordResetEmail(emailAddress).then(function() {
        alert("you can chenk your E-mail" );

        firebase.auth().signOut()
        .then(function(result){
            window.location.assign("index.html");
        })
        .catch(function(error) {
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error", errorMessage);
        });
    }).catch(function(error) {
    });

}
