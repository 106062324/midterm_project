function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var signIn = document.getElementById('sign-in');
    var googleSignIn = document.getElementById('google-sign-in');
    var btnSignUp = document.getElementById('btnSignUp');

    signIn.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            window.location.href="chatroom.html"
        }).catch(function(error) {
            // Handle Errors here.
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            console.log("catch");
        });
    });

    googleSignIn.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            // The signed-in user info.
            var user = result.user;
            // ...
            window.location.href="chatroom.html";
        }).catch(function(error) {
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            create_alert("error", errorMessage);
        });
    });

    var contactsRef = firebase.database().ref('contact_list');
    
    btnSignUp.addEventListener('click', function () {        
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function(result) {
            var contact ={
                email: txtEmail.value,
                userName: "",
                status: "online"
            };
            contactsRef.push(contact);
            create_alert("success", "");

        }).catch(function(error) {
            // Handle Errors here.
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};